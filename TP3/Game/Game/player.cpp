#include "player.h"
#include <string>
#include <utility>
#include <random>
#include <functional>

Player::Player(std::string name,std::string sym): _name(std::move(name)), _sym(std::move(sym)), _score(0){}

HPlayer::HPlayer(std::string name,std::string sym): Player(std::move(name),std::move(sym)){}

std::pair<int, int> HPlayer::Play() const {
    int x,y;
    printf("x: ");
    scanf("%d",&x);
    printf("y: ");
    scanf("%d",&y);
    return {x,y};
}

int HPlayer::PlayByLines() const {
    int l;
    printf("x: ");
    scanf("%d",&l);
    return l;
}

int HPlayer::PlayByCols()  const {
    int c;
    printf("y: ");
    scanf("%d",&c);
    return c;
}

std::string Player::GetName() const {
    return _name;
}

Symbol Player::GetSymbol() const {
    return _sym;
}

std::pair<int, int> CPlayer::Play() const {
    std::random_device r;
    auto gen = std::bind(std::uniform_int_distribution<>(0,7),std::default_random_engine(r())); // ok c'est horrible
    return {gen(),gen()};
}

int CPlayer::PlayByLines() const {
    std::random_device r;
    auto gen = std::bind(std::uniform_int_distribution<>(0,7),std::default_random_engine(r())); // ok c'est horrible
    return gen();
}

int CPlayer::PlayByCols() const {
    std::random_device r;
    auto gen = std::bind(std::uniform_int_distribution<>(0,7),std::default_random_engine(r())); // ok c'est horrible
    return gen();
}

CPlayer::CPlayer(): Player("Com","H"){}
