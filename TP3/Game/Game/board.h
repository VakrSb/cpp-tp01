#ifndef BOARD_H_
#define BOARD_H_
#include "player.h"
#include <array>
#include <cstdint>
#include <optional>
#include <string>
#include <map>
#include <vector>
#include "common.h"

typedef int Cell;

class Board {
public:
  const static Cell EMPTY_CELL;
  Board(int,int);
  Cell GetCell(int, int) const;
  Cell GetCell(int) const;
  bool SetCell(int, int, const Symbol& sym);
  void Draw() const;
  Cell RegisterSymbol(const Symbol&);
  bool isFull() const;
  int GetHeight() const;
  int GetWidth() const;

private:
  int GetIndex(int, int) const;
  Symbol GetSymbol(Cell) const;

  const int _width;
  const int _height;
  std::vector<Cell> _cells;
  //std::map<Cell,Symbol> _cell_to_symbol;
  std::vector<Symbol> _cs;
  std::map<Symbol ,Cell> _symbol_to_cell;
  Cell prev_cell = 0;
};

#endif // BOARD_H_
