#include "board.h"
#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <string>

const Cell Board::EMPTY_CELL = 0;

Board::Board(int w, int h): _width(w), _height(h) {
    _cells = std::vector<Cell>(_width * _height);
    //_cell_to_symbol = std::map<Cell, Symbol>();
    _symbol_to_cell = std::map<Symbol,Cell>();
    _cs = std::vector<Symbol>();
    RegisterSymbol(" ");
    std::fill(_cells.begin(), _cells.end(),Board::EMPTY_CELL);
}

Cell Board::RegisterSymbol(const Symbol & sym) {
    Cell cell = _cs.size();
    //_cell_to_symbol[cell] = sym;
    _symbol_to_cell[sym] = cell;
    _cs.push_back(sym);
    return cell;
}

Symbol Board::GetSymbol(Cell cell) const {
    return _cs[cell];
}

int Board::GetIndex(int x, int y) const {
    return y * _width + x;
}

Cell Board::GetCell(int x, int y) const {
    return GetCell(GetIndex(x,y));
}

void Board::Draw() const {
  std::cout << "#############" << std::endl;
  auto print = [](const int &n) { std::cout << " " << n; };
  for (int y = 0; y < _height; y++) {
    for (int x = 0; x < _width; x++) {
      std::cout << "|" << GetSymbol(GetCell(x,y));
      //std::cout << "   ";
    }
    std::cout << "|" << std::endl;
  }
  std::cout << "#############" << std::endl;
}

bool Board::SetCell(int x, int y,const Symbol& sym) {
    if(y == -1){
        bool found = false;
        for(int i = _height - 1 ; i >= 0; i--) {
            if(GetCell(x,i) == EMPTY_CELL){
                found = true;
                y = i;
                break;
            }
        }
        if (!found) return false;
    }

    if(x == -1){
        bool found = false;
        for(int i = _width - 1 ; i >= 0; i--) {
            if(GetCell(i,y) == EMPTY_CELL){
                found = true;
                x = i;
                break;
            }
        }
        if (!found) return false;
    }

    if(!_symbol_to_cell.contains(sym)) {
        return false;
    }
    if(GetIndex(x,y) >= _cells.size() || GetIndex(x,y) < 0) return false;
    if(_cells[GetIndex(x,y)] != EMPTY_CELL){
        return false;
    }
    _cells[GetIndex(x,y)] = _symbol_to_cell[sym];
    return true;
}

Cell Board::GetCell(int index) const {
    if(index >= _cells.size() || index < 0) return {};
    return _cells[index];
}

bool Board::isFull() const {
    for(auto c : _cells){
        if (c == 0) return false;
    }
    return true;
}

int Board::GetHeight() const {
    return _height;
}

int Board::GetWidth() const {
    return _width;
}

