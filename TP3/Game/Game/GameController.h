//
// Created by vakrsb on 11/9/22.
//

#ifndef MORPION_GAMECONTROLLER_H
#define MORPION_GAMECONTROLLER_H

#include <vector>
#include <memory>
#include <functional>
#include "board.h"
#include "player.h"

class GameController {
public:
    enum InputType {NORM,COLS,LINE};
    explicit GameController(std::function<bool(const Board&)>, std::initializer_list<std::shared_ptr<Player>>,int,int);
    void SetInputType(InputType);
    void Run();
private:
    std::function<bool(const Board&)> _victory_rule;
    Board _board;
    std::vector<std::shared_ptr<Player>> _players;
    InputType _type;
};

#endif //MORPION_GAMECONTROLLER_H
