#ifndef PLAYER_H_
#define PLAYER_H_
#include <string>
#include <utility>
#include "board.h"
#include "common.h"

class Player {
public:
    Player(std::string, std::string);
    virtual std::pair<int,int> Play() const = 0;
    virtual int PlayByLines() const = 0;
    virtual int PlayByCols()  const = 0;
    std::string GetName() const;
    Symbol GetSymbol() const;

protected:
    std::string _name;
    Symbol _sym;
    int _score;
};

class CPlayer : public Player {
public:
    CPlayer();
    std::pair<int,int> Play() const override;
    int PlayByLines() const override;
    int PlayByCols() const override;
};

class HPlayer : public Player {
public:
    HPlayer(std::string name, std::string sym);
    std::pair<int,int> Play() const override;
    int PlayByLines() const override;
    int PlayByCols() const override;
};

#endif // PLAYER_H_
