//
// Created by vakrsb on 11/9/22.
//
#include "GameController.h"
#include <utility>
#include <iostream>

GameController::GameController(std::function<bool(const Board &)> rule, std::initializer_list<std::shared_ptr<Player>> players, int w,int h): _victory_rule(std::move(rule)),_board(Board(w,h)), _players(players) {
    for(const auto & player : _players){
        _board.RegisterSymbol(player->GetSymbol());
    }
    _type = NORM;
}

void GameController::Run() {
    int round_count = 1;
    start:
    while(true){
        system("clear");
        std::cout << "######### Round " << round_count << " #########" << std::endl;
        for(const auto & _player : _players){
            std::cout << _player->GetName() << "'s turn ! (" << _player->GetSymbol() << ")" << std::endl;
            _board.Draw();
            std::pair<int,int> coords;
            bool first = true;
            do {
                if (!first) std::cout << "Invalid Input" << std::endl;
                first=false;
                switch (_type) {
                    case NORM: coords = _player->Play();
                        break;
                    case COLS: coords = {-1,_player->PlayByCols()};
                        break;
                    case LINE: coords = {_player->PlayByLines(), -1};
                        break;
                }
            }while(!_board.SetCell(coords.first,coords.second,_player->GetSymbol()));
            if(_victory_rule(_board)){
                _board.Draw();
                std::cout << "Player : " << _player->GetName() << " wins !";
                return;
            }
            if(_board.isFull()) {
                char c;
                printf("do you want to continue ? N/y: ");
                scanf(" %c",&c);
                if (c == 'y' || c == 'Y') goto start;
                return;
            }
        }
        round_count++;
    }
}

void GameController::SetInputType(InputType type) {
    _type = type;
}
