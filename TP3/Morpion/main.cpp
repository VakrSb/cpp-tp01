//
// Created by vakrsb on 11/9/22.
//

#include "GameController.h"

bool rule(const Board& board){
    int win[8][3] = {{0, 1, 2}, // Check first row.
                     {3, 4, 5}, // Check second Row
                     {6, 7, 8}, // Check third Row
                     {0, 3, 6}, // Check first column
                     {1, 4, 7}, // Check second Column
                     {2, 5, 8}, // Check third Column
                     {0, 4, 8}, // Check first Diagonal
                     {2, 4, 6}}; // Check second Diagonal
    // Check all possible winning combinations
    for (auto & i : win){
        auto a = board.GetCell(i[0]);
        auto b = board.GetCell(i[1]);
        auto c = board.GetCell(i[2]);
        if (a == b && b == c && a != 0){
            return true;
        }
    }
    return false;
}

int main(){
    auto player = {
        std::shared_ptr<Player>(new HPlayer("A","X")),
        std::shared_ptr<Player>(new CPlayer()),
    };
    GameController gameController = GameController(rule, player,3,3);
    gameController.Run();
}