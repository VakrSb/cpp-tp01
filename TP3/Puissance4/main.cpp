//
// Created by vakrsb on 11/9/22.
//
#include "GameController.h"

bool rule(const Board& board){
    // horizontalCheck
    for (int j = 0; j<board.GetHeight()-3 ; j++ ){
        for (int i = 0; i<board.GetWidth(); i++){
            auto a = board.GetCell(i,j);
            auto b = board.GetCell(i,j+1);
            auto c = board.GetCell(i,j+2);
            auto d = board.GetCell(i,j+3);
            if (a == b && b == c && c == d && d != 0){
                return true;
            }
        }
    }
    // verticalCheck
    for (int i = 0; i<board.GetWidth()-3 ; i++ ){
        for (int j = 0; j<board.GetHeight(); j++){
            auto a = board.GetCell(i,j);
            auto b = board.GetCell(i+1,j);
            auto c = board.GetCell(i+2,j);
            auto d = board.GetCell(i+3,j);
            if ( a == b && b == c && c == d && d != 0){
                return true;
            }
        }
    }
    // ascendingDiagonalCheck
    for (int i=3; i<board.GetWidth(); i++){
        for (int j=0; j<board.GetHeight()-3; j++){
            if (board.GetCell(i,j) == board.GetCell(i-1,j+1) && board.GetCell(i-1,j+1) == board.GetCell(i-2,j+2) && board.GetCell(i-2,j+2) == board.GetCell(i-3,j+3) && board.GetCell(i-3,j+3) != 0)
                return true;
        }
    }
    // descendingDiagonalCheck
    for (int i=3; i<board.GetWidth(); i++){
        for (int j=3; j<board.GetHeight(); j++){
            if (board.GetCell(i,j) == board.GetCell(i-1,j-1) && board.GetCell(i-1,j-1) == board.GetCell(i-2,j-2)  && board.GetCell(i-2,j-2) == board.GetCell(i-3,j-3) && board.GetCell(i-3,j-3) != 0)
                return true;
        }
    }
    return false;
}

int main(){
    auto player = {
            std::shared_ptr<Player>(new HPlayer("A","X")),
            std::shared_ptr<Player>(new CPlayer()),
    };
    GameController gameController = GameController(rule, player,7,4);
    gameController.SetInputType(GameController::LINE);
    gameController.Run();
}
