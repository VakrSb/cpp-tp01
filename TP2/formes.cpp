#include "formes.hpp"
#include "build/_deps/fmt-src/include/fmt/core.h"
#include <array>
#include <cmath>
#include <cstdio>
#include <functional>
#include <iostream>
#include <math.h>
#include <numbers>
#include <fmt/core.h>
#include <string>

void Point::Afficher() const { std::cout << ToString() << std::endl; }

std::string Point::ToString() const {
  return fmt::format("Point [x: {}, y: {}]", x, y);
}

Rectangle::Rectangle(const Point &point, int longueur, int largeur)
    : _point(point), _width(longueur), _height(largeur) {}

std::string Rectangle::ToString() const {
  return fmt::format(
      "Point [point: {}, width: {}, height: {}, perimeter: {}, surface: {}]",
      _point.ToString(), _width, _height, GetPerimeter(), GetSurface());
}

void Rectangle::Afficher() const { std::cout << ToString() << std::endl; }

inline void Rectangle::SetPoint(const Point &point) { this->_point = point; }

Cercle::Cercle(const Point &point, int diametre)
    : _center(point), _diametre(diametre) {}

std::string Cercle::ToString() const {
  return fmt::format(
      "Cercle [center: {}, diameter: {}, perimeter: {}, surface: {}]",
      _center.ToString(), GetDiametre(), GetPerimetre(), GetSurface());
}
void Cercle::Afficher() const { std::cout << ToString() << std::endl; }

Triangle::Triangle(const std::array<Point, 3> &points) : _points(points) {}

std::string Triangle::ToString() const {
  return fmt::format(R"(Triangle [points:
  A {}
  B {}
  C {},
  base: {},
  height: {},
  surface {},
  length [
    AB: {},
    BC: {},
    AC: {}
],
  isIsosceles: {},
  isRectangle: {},
  isEquilateral: {}
])",
                     _points[0].ToString(), _points[1].ToString(), _points[2].ToString(),
                     GetBase(),
                     GetHeight(),
                     GetSurface(),
                     Point::Distance(_points[0], _points[1]),
                     Point::Distance(_points[1], _points[2]),
                     Point::Distance(_points[0], _points[2]),
                     isIsosceles(),
                     isRectangle(),
                     isEquilateral());
}

void Triangle::Afficher() const { std::cout << ToString() << std::endl; }

std::array<double, 3> Triangle::GetLengths() const {
  std::array<double, 3> longueurs = std::array<double, 3>();
  longueurs[0] = Point::Distance(_points[0], _points[1]);
  longueurs[1] = Point::Distance(_points[0], _points[2]);
  longueurs[2] = Point::Distance(_points[1], _points[2]);
  return longueurs;
}

double Triangle::GetBase() const {
  std::array<double, 3> longueurs = std::array<double, 3>();
  longueurs[0] = Point::Distance(_points[0], _points[1]);
  longueurs[1] = Point::Distance(_points[0], _points[2]);
  longueurs[2] = Point::Distance(_points[1], _points[2]);
  return *std::max_element(longueurs.begin(), longueurs.end());
}

double Triangle::GetSurface() const {
  double ab = Point::Distance(_points[0], _points[1]);
  double ac = Point::Distance(_points[0], _points[2]);
  double bc = Point::Distance(_points[1], _points[2]);
  double s = this->GetPerimeter() / 2;
  return sqrt(s * (s - ab) * (s - ac) * (s - bc));
}

double Triangle::GetPerimeter() const {
  double ab = Point::Distance(_points[0], _points[1]);
  double ac = Point::Distance(_points[0], _points[2]);
  double bc = Point::Distance(_points[1], _points[2]);
  return ab + ac + bc;
}

double Triangle::GetHeight() const {
  double surface = this->GetSurface();
  double base = this->GetBase();
  return (surface * 2) / base;
}

bool Triangle::isIsosceles() const {
  double ab = Point::Distance(_points[0], _points[1]);
  double ac = Point::Distance(_points[0], _points[2]);
  double bc = Point::Distance(_points[1], _points[2]);
  return cmpf(ab, bc) || cmpf(bc, ac) || cmpf(ac, ab);
}

bool Triangle::isRectangle() const {
  double ab = Point::Distance(_points[0], _points[1]);
  double ac = Point::Distance(_points[0], _points[2]);
  double bc = Point::Distance(_points[1], _points[2]);
  return cmpf(ab * ab + bc * bc, ac * ac) || cmpf(bc * bc + ac * ac, ab * ab) ||
         cmpf(ac * ac + ab * ab, bc * bc);
};

bool Triangle::isEquilateral() const {
  double ab = Point::Distance(_points[0], _points[1]);
  double ac = Point::Distance(_points[0], _points[2]);
  double bc = Point::Distance(_points[1], _points[2]);
  return cmpf(ab, ac) && cmpf(ac, bc) && cmpf(bc, ab);
};
