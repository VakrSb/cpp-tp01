#include <array>
#include <iostream>
#include "formes.hpp"
#include <cassert>

using std::array;


void RectangleTests() {
#define WIDTH 100
#define HEIGHT 100
#define POINT_X 5
#define POINT_Y 5
#define PERIMETER 2 * (WIDTH + HEIGHT)
#define SURFACE WIDTH * HEIGHT
  auto r = Rectangle(Point(5,5), WIDTH, HEIGHT);
  r.Afficher();

  assert(r.GetWidth() == WIDTH);
  assert(r.GetHeight() == HEIGHT);

  auto point = r.GetPoint();
  assert(point.x == POINT_X);
  assert(point.y == POINT_Y);

  assert(r.GetPerimeter() == PERIMETER);

  assert(r.GetSurface() == SURFACE);

  auto biggerRectangle = Rectangle({POINT_X,POINT_Y},WIDTH + 1, HEIGHT);

  assert(r.BiggerPerimeter(biggerRectangle) == false);
  assert(r.BiggerSurface(biggerRectangle) == false);

#undef WIDTH
#undef HEIGHT
#undef POINT_X
#undef POINT_Y
#undef PERIMETER
#undef SURFACE
}

void CercleTests(){
#define DIAM 100
#define POINT_X 5
#define POINT_Y 5
#define PERIMETER 2 * M_PI * (static_cast<float>(DIAM)/2.0f)
#define SURFACE M_PI * DIAM/2 * DIAM/2

  auto c = Cercle({POINT_X,POINT_Y},DIAM);
  c.Afficher();

  auto point = c.GetCenter();
  assert(point.x == POINT_X);
  assert(point.y == POINT_Y);

  assert(c.GetDiametre() == DIAM);

  assert(cmpf(c.GetPerimetre(), PERIMETER));

  assert(cmpf(c.GetSurface(), SURFACE));

#undef DIAM
#undef POINT_X
#undef POINT_Y
#undef PERIMETER
#undef SURFACE
}

void TriangleTests(){
#define POINTA_X 0
#define POINTA_Y 0

#define POINTB_X 0
#define POINTB_Y 10

#define POINTC_X 10
#define POINTC_Y 0

  std::array<Point, 3> points {
  Point(POINTA_X,POINTA_Y),
  Point(POINTB_X,POINTB_Y),
  Point(POINTC_X,POINTC_Y),
};

 auto t = Triangle(points);

 t.Afficher();

#undef POINTA_X
#undef POINTA_Y
#undef POINTB_X
#undef POINTB_Y
#undef POINTC_X
#undef POINTC_Y
}

int main(void) {
  RectangleTests();
  CercleTests();
  TriangleTests();
  return 0;
}
