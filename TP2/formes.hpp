#ifndef POINT_H_
#define POINT_H_

#include <cmath>
#include <iostream>
#include <math.h>
#include <ostream>
#include <array>
#include <algorithm>

inline bool cmpf(float A, float B, float epsilon = 0.005f)
{
    return (fabs(A - B) < epsilon);
}

struct Point {
  float x, y;
  Point(float _x, float _y) : x(_x), y(_y) {}
  static double Distance(const Point &a, const Point &b) {
    return std::sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2));
  }
  void Afficher() const;
  std::string ToString() const;
};

class Rectangle {
public:
  Rectangle(const Point&, int, int);
  const Point &GetPoint() const;
  void SetPoint(const Point &) ;
  int GetHeight() const;
  void SetHeight(int);
  int GetWidth() const;
  void SetWidth(int);
  int GetPerimeter() const;
  int GetSurface() const;
  bool BiggerPerimeter(const Rectangle &) const;
  bool BiggerSurface(const Rectangle &) const;
  void Afficher() const;
  std::string ToString() const;

private:
  Point _point;
  int _width, _height;
};

inline int Rectangle::GetHeight() const{
    return this->_height;
}
inline void Rectangle::SetHeight(int largeur) {
    this->_height = largeur;
}
inline int Rectangle::GetWidth() const{
    return this->_width;
}
inline void Rectangle::SetWidth(int longueur) {
    this->_width = longueur;
}
inline int Rectangle::GetPerimeter() const{
 return 2*(this->_height + this->_width);
}
inline int Rectangle::GetSurface() const{
    return this->_height * this->_width;
}
inline const Point& Rectangle::GetPoint() const{
    return this->_point;
}

inline bool Rectangle::BiggerPerimeter(const Rectangle & rect) const {
 return this->GetPerimeter() > rect.GetPerimeter();
}

inline bool Rectangle::BiggerSurface(const Rectangle & rect) const {
 return this->GetSurface() > rect.GetSurface();
}

class Cercle {
public:
  Cercle(const Point &, int);
  const Point &GetCenter() const;
  void SetCenter(const Point &);
  int GetDiametre() const ;
  void SetDiametre(int);
  float GetPerimetre() const;
  float GetSurface() const;
  bool IsOnCircle(const Point &) const;
  bool IsInCircle(const Point &) const;
  void Afficher() const;
  std::string ToString() const;

private:
  Point _center;
  int _diametre;
};

inline const Point& Cercle::GetCenter() const{
    return _center;
}
inline int Cercle::GetDiametre() const {
  return _diametre;
}
inline void Cercle::SetCenter(const Point& point){
    _center = point;
}
inline void Cercle::SetDiametre(int diametre){
    _diametre = diametre;
}
inline float Cercle::GetPerimetre() const {
    return 2 * M_PI * (static_cast<float>(_diametre)/2.0f);
}
inline float Cercle::GetSurface() const{
   return M_PI * pow((static_cast<float>(_diametre)/2.0f),2);
}
inline bool Cercle::IsInCircle(const Point &point) const {
  return Point::Distance(point, _center) < _diametre;
}
inline bool Cercle::IsOnCircle(const Point &point) const {
  return Point::Distance(point, _center) == _diametre;
}


class Triangle {
public:
  Triangle(const std::array<Point, 3> &);
  void SetPoints(const std::array<Point, 3> &);
  const std::array<Point, 3> &GetPoints() const;
  // const std::array<Point, 2> GetBase() const;
  double GetBase() const;
  double GetHeight() const;
  double GetPerimeter() const;
  double GetSurface() const;
  std::array<double, 3> GetLengths() const;
  bool isIsosceles() const;
  bool isRectangle() const;
  bool isEquilateral() const;
  void Afficher() const;
  std::string ToString() const;

private:
    std::array<Point,3> _points;
};

inline void Triangle::SetPoints(const std::array<Point, 3> & points){
  _points = points;
}

inline const std::array<Point, 3>& Triangle::GetPoints() const {
  return _points;
}

#endif // POINT_H_
