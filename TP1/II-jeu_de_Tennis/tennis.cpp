#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <random>
#include <functional>
#include <vector>

#define print_bits(x)                                            \
  do {                                                           \
    unsigned long long a__ = (x);                                \
    size_t bits__ = sizeof(x) * 8;                               \
    printf(#x ": ");                                             \
    while (bits__--) putchar(a__ &(1ULL << bits__) ? '1' : '0'); \
    putchar('\n');                                               \
  } while (0)

#define PLAYER_A 0
#define PLAYER_B 1


static const int SCORES_VALUES[4] = {0,15,30,40};
#define MAX_SCORE 3

typedef uint8_t score;

void setVictory(score* score){
    *score |= 8 << 0; // set 4th bit
}

bool getVictory(score* score){
    return *score & (1<< 3);
}

void setAdvantage(score* score){
    *score |= 4 << 0;
}

bool getAdvantage(score* score){
    return *score & (1 << 2);
}

uint8_t getScore(score* score){
    return (*score & 0x7);
}

uint8_t addScore(score* score){
    bool hasVic = getVictory(score);
    bool hasAdv = getAdvantage(score);

    uint8_t new_score = getScore(score) + 1 ;
    if(hasVic) setVictory(&new_score);
    if(hasAdv) setAdvantage(&new_score);
    return new_score;
}

void update_scores(score scores[],int winner){

    scores[winner] = addScore(&scores[winner]);
    uint8_t score_winner = getScore(&scores[winner]);
    uint8_t score_loser =  getScore(&scores[!winner]);

    if(score_winner == MAX_SCORE && score_loser < MAX_SCORE){
        setVictory(&scores[winner]);
        return;
    }

    else if(score_winner == 40 && score_winner == score_loser){

        if(getAdvantage(&scores[winner])){
            setVictory(&scores[winner]);
            return;
        }
        setAdvantage(&scores[winner]);
    }
}

int main(void) {
    std::random_device r;
 auto gen = std::bind(std::uniform_int_distribution<>(0,1),std::default_random_engine(r()));

 score scores[2];

 while(true){
    int winner = gen();
    std::cout << winner << std::endl;
    update_scores(scores,winner);
    if(getVictory(&scores[PLAYER_A])){
       std::cout << "Player A wins " << SCORES_VALUES[MAX_SCORE] << "-" << SCORES_VALUES[scores[PLAYER_B]] << std::endl;
       break;
    }
    else if(getVictory(&scores[PLAYER_B])){
       std::cout << "Player B wins " << SCORES_VALUES[MAX_SCORE] << "-" << SCORES_VALUES[scores[PLAYER_A]] << std::endl;
       break;
    }
 }
}
