#include <iostream>
#include <random>
#include <functional>

#define TEXT "## Find the number ##"

// III.2.1 & III.2.2
int main(void) {
  std::random_device r;
  auto gen = std::bind(std::uniform_int_distribution<>(0, 1000),
                       std::default_random_engine(r()));

  std::cout << TEXT << std::endl;

  int num = gen();
  do {
    int guess;
    std::cout << "Take a guess..." << std::endl;
    std::cin >> guess;

    if (guess == num) {
      std::cout << "Congratulations you guessed le right number ! (" << num << ")" << std::endl;
      break;
    }

    if (guess > num)
      std::cout << "you're too high" << std::endl;
    else
      std::cout << "you're too low" << std::endl;
  } while (true);
}
