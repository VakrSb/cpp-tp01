#include <cstdlib>
#include <functional>
#include <iostream>
#include <random>

#define LUP 1000
#define LDOWN 0

// III.2.3
int main(void) {
  std::random_device r;
  auto gen = std::bind(std::uniform_int_distribution<>(LDOWN, LUP),
                       std::default_random_engine(r()));
  int num;
  std::cout << "Choose a number " << std::endl;
  std::cin >> num;

  bool isFound = false;
  int right = LUP;
  int left = LDOWN;
  do {
    int num_guessed = (right + left) / 2;
    std::system("clear");
    std::cout << "Is it " << num_guessed << " ?" << std::endl << "(your number is " << num << ")";

    std::cout << R"(
1 - too high\n
2 - too low\n
3 - right number\n
)" << std::endl;
    int ans;

    std::cin >> ans;

    if (ans == 3)
      break;

    if (ans == 2)
      left = num_guessed;
    else right = num_guessed;

  } while (!isFound);
  std::cout << "yay !" << std::endl;
}
