#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include <random>
#include <string>

// trim from start
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

// III.1.1 & III.1.2
int main(void) {

  // III.1.1
  // std::cout << "Prénom: ";
  // std::cin >> firstname;

  // std::cout << "Hello " << firstname << " !" << std::endl;

  // III.1.2
  std::cout << "Prénom Nom (separated by a `/`): ";

  std::string input;
  std::getline(std::cin, input);

  char delimiter = '/';
  std::string firstname = input.substr(0,input.find(delimiter));
  std::string lastname = input.substr(input.find(delimiter) + 1,input.size()- 1);

  if (!std::cin.fail() && !firstname.empty() &&
      !lastname.empty() && input.find(delimiter) != std::string::npos) {

    trim(firstname);
    trim(lastname);

    bool isspace = false;
    std::for_each(firstname.begin(), firstname.end(),
                  [&](char &c) {
                    if(isspace){
                      c = ::toupper(c);
                      isspace = false;
                    }
                    else c = ::tolower(c);
                    if(c == ' ') isspace = true;
                  });

    std::for_each(lastname.begin(), lastname.end(),
                  [&](char &c) {
                    if(isspace){
                      c = ::toupper(c);
                      isspace = false;
                    }
                    else c = ::tolower(c);
                    if(c == ' ') isspace = true;
                  });

    firstname[0] = toupper(firstname[0]);
    lastname[0] = toupper(lastname[0]);

    std::cout << "Hello " << firstname << " " << lastname << std::endl;
  } else {
    std::cout << "Wrong input format" << std::endl;
    exit(EXIT_FAILURE);
  }
}
