#include <cctype>
#include <cstdio>
#include <iostream>
#include <ostream>
#include <random>
#include <vector>

// I.1.1
int somme(int a, int b) { return a + b; }

// I.1.2
void inverse(int &a, int &b) {
  int temp = a;
  a = b;
  b = temp;
}

// I.1.3
void sommeRef(int a, int b, int &sum) { sum = a + b; }

int main(void) {
  int a = 10, b = 11;
  std::cout << "I.1.1, somme(5,5) = " << somme(a, b) << std::endl;

  inverse(a, b);
  std::cout << "I.1.2, inverse(10,11) = " << a << " :: " << b << std::endl;

  int somme;
  sommeRef(a, b, somme);
  std::cout << "I.1.3, sommeRef(11,10) = " << somme << std::endl;

 }
