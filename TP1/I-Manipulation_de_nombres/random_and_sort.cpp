#include <iostream>
#include <random>

#define SIZE 10

void inverse(int &a, int &b) {
  int temp = a;
  a = b;
  b = temp;
}

std::vector<int> randomArray(std::default_random_engine generator,
                             std::uniform_int_distribution<int> distribution,
                             int size) {
  std::vector<int> vec;
  for (int i = 0; i < size; i++) {
    vec.push_back(distribution(generator));
  }
  return vec;
}

void printArray(const std::vector<int> &array) {
  std::cout << "[ ";
  for (auto num : array) {
    printf("%02d ", num);
  }
  std::cout << "]" << std::endl;
}

void sortAscArray(std::vector<int> &array) {
  for (int i = 0; i < array.size() - 1; ++i) {
    for (int j = 0; j < array.size() - i - 1; ++j) {
      if (array[j] > array[j + 1])
        inverse(array[j], array[j + 1]);
    }
  }
}

void sortDescArray(std::vector<int> &array) {
  for (int i = 0; i < array.size() - 1; ++i) {
    for (int j = 0; j < array.size() - i - 1; ++j) {
      if (array[j] < array[j + 1])
        inverse(array[j], array[j + 1]);
    }
  }
}

int main(void) {
  std::random_device r;
  std::default_random_engine generator(r());
  std::uniform_int_distribution<int> distribution(0, 100);

  int size;
  bool asc;
  std::cout << "Enter size: ";
  std::cin >> size;

  std::cout << R"(
0 - desc\n
1 - asc\n
)" << std::endl;
  std::cin >> asc;

  std::cout << std::endl;

  auto array = randomArray(generator, distribution, size);
  std::cout << "unsorted: ";
  printArray(array);
  if (asc) {
    sortAscArray(array);
    std::cout << "sorted: ";
    printArray(array);
    std::cout << "##################" << std::endl;
    sortDescArray(array);
    std::cout << "inversed: ";
    printArray(array);
  } else {
    sortDescArray(array);
    std::cout << "sorted: ";
    printArray(array);
    std::cout << "##################" << std::endl;
    sortAscArray(array);
    std::cout << "inversed: ";
    printArray(array);
  }
}
